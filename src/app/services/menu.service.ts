import {Injectable} from '@angular/core'


@Injectable()
export class MenuService {

    items:any[] 

    constructor(){
        this.items = [
            {activated:true, nombre:"Inicio"},
            {activated:false, nombre:"Historio"},
            {activated:false, nombre:"Misión"},
            {activated:false, nombre:"Visión"},
        ]
    }

    getItems(){
        return this.items
    }

}