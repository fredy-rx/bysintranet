import { Component, OnInit } from '@angular/core'
import { MenuService } from '../../../services/menu.service'

@Component({
    selector: "menu",
    templateUrl: "menu.component.html",
    providers: [MenuService]
})
export class MenuComponent implements OnInit{
    list:any[]

    constructor(private menuService: MenuService){
    }

    ngOnInit(){
        this.list = this.menuService.getItems()
    }
}