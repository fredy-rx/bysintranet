import { Component, OnInit, Input } from '@angular/core'

@Component({
    selector: "menu-item",
    templateUrl: "./menuitem.component.html"
})
export class MenuItemComponent implements OnInit{

    @Input() activated: boolean
    @Input() nombre: string

    ngOnInit(){ }
}