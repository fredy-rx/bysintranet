import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HeaderComponent } from './components/header/header.component'

import { MenuComponent } from './components/commons/menu/menu.component'
import { MenuItemComponent } from './components/commons/menu-item/menuitem.component'
import { BrandComponent } from './components/commons/brand.component'
import { NavbarComponent } from './components/navbar/navbar.component'
@NgModule({
  declarations: [
    AppComponent, HeaderComponent, MenuComponent, MenuItemComponent,
    BrandComponent, NavbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
